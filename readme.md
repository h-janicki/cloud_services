# Cloud Services technical evaluation task

## Local setup:

1. Install Docker. If you are using MS Windows, don't forget to install WSL2, Ubuntu.
2. Make sure the Docker engine is running.
3. Build the app by going to project root folder and typing in:
    ```
    docker build . -t web
    ```
4. To start the app once it's built type in:
    ```
    docker run -i -t -p 8000:8000 -v $PWD/:/home/app web
    ```