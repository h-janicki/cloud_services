from django.db import models


class Menu(models.Model):
    name = models.CharField(unique=True, max_length=50)
    description = models.TextField()

    items = models.ManyToManyField("MenuItem")

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class MenuItem(models.Model):
    name = models.CharField(unique=True, max_length=50)
    description = models.TextField()
    price = models.PositiveIntegerField()  # cents
    preparation_time = models.PositiveIntegerField()  # seconds
    is_vegetarian = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
