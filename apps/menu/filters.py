import django_filters
from django.db.models import Count

from apps.menu.models import Menu


class MenuFilterSet(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr='icontains')
    created__gt = django_filters.DateTimeFilter(field_name="created", lookup_expr='gt')
    created__lt = django_filters.DateTimeFilter(field_name="created", lookup_expr='lt')

    updated__gt = django_filters.DateTimeFilter(field_name="updated", lookup_expr='gt')
    updated__lt = django_filters.DateTimeFilter(field_name="updated", lookup_expr='lt')

    order_by = django_filters.OrderingFilter(fields=("name", "created", "updated", "items_count"))

    class Meta:
        model = Menu
        fields = ["name", "created", "updated", "items"]

    def __init__(self, *args, **kwargs):
        order_by = kwargs["request"].GET.get("order_by", '')
        if order_by == "items_count":
            kwargs["queryset"] = kwargs["queryset"].annotate(items_count=Count("items"))
            kwargs["queryset"] = kwargs["queryset"].order_by("items_count")
        if order_by == "-items_count":
            kwargs["queryset"] = kwargs["queryset"].annotate(items_count=Count("items"))
            kwargs["queryset"] = kwargs["queryset"].order_by("-items_count")
        super().__init__(*args, **kwargs)
