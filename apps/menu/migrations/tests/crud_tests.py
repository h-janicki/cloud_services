from datetime import datetime
from typing import Generator
from unittest import mock

from rest_framework import status
import pytest

from apps.menu.models import Menu, MenuItem
from apps.menu.serializers import MenuSerializer, MenuDetailSerializer

frozen_time_str = "2023-02-08T15:02:03Z"
frozen_time_dt = datetime.strptime(frozen_time_str, "%Y-%m-%dT%H:%M:%SZ")


@pytest.fixture
def fixture__menu_item__data() -> list:
    return [
        {
            "id": 1, "name": "test_name_1", "description": "test_description_1", "price": 50, "preparation_time": 50,
            "is_vegetarian": True, "created": frozen_time_str, "updated": frozen_time_str
        },
        {
            "id": 2, "name": "test_name_2", "description": "test_description_2", "price": 50, "preparation_time": 50,
            "is_vegetarian": True, "created": frozen_time_str, "updated": frozen_time_str
        },
    ]


@pytest.fixture
def fixture__menu__data() -> dict:
    return {
        "id": 1, "name": "test_name", "description": "test_description",
        "items": [1, 2], "created": frozen_time_str, "updated": frozen_time_str
    }


@pytest.fixture
def fixture__menu_item(fixture__menu_item__data) -> None:
    create_data = fixture__menu_item__data.copy()
    for menu_item_data in create_data:
        with mock.patch('django.utils.timezone.now', mock.Mock(
             return_value=datetime.strptime(menu_item_data["created"], "%Y-%m-%dT%H:%M:%SZ"))
        ):
            MenuItem.objects.create(**menu_item_data)
    yield


@pytest.fixture
def fixture__menu(fixture__menu_item, fixture__menu__data) -> Generator[Menu, None, None]:
    create_data = fixture__menu__data.copy()
    items = create_data.pop("items", None)
    with mock.patch('django.utils.timezone.now', mock.Mock(return_value=frozen_time_dt)):
        menu = Menu.objects.create(**create_data)
    if items:
        menu.items.set(items)
    yield menu


@pytest.mark.django_db
class UserViewSetCRUDTest:

    def test__list_view__without_authentication(self, api_client, fixture__menu):
        response = api_client.get("/menu/menu/")
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [MenuSerializer(fixture__menu).data]

    def test__list_view__with_authentication(self, api_client, fixture__user, fixture__menu):
        api_client.force_authenticate(user=fixture__user)
        response = api_client.get("/menu/menu/")
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [MenuSerializer(fixture__menu).data]

    def test__retrieve_view__without_authentication(self, api_client, fixture__menu):
        response = api_client.get(f"/menu/menu/{fixture__menu.id}/")
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == MenuDetailSerializer(fixture__menu).data

    def test__retrieve_view__with_authentication(self, api_client, fixture__user, fixture__menu):
        api_client.force_authenticate(user=fixture__user)
        response = api_client.get(f"/menu/menu/{fixture__menu.id}/")
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == MenuDetailSerializer(fixture__menu).data

    def test__create_view__without_authentication(self, api_client, fixture__menu__data):
        assert not Menu.objects.exists()
        response = api_client.post("/menu/menu/", fixture__menu__data, format="json")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert not Menu.objects.exists()

    @mock.patch('django.utils.timezone.now', mock.Mock(return_value=frozen_time_dt))
    def test__create_view__with_authentication(self, api_client, fixture__user, fixture__menu_item, fixture__menu__data):
        assert not Menu.objects.exists()
        api_client.force_authenticate(user=fixture__user)
        response = api_client.post("/menu/menu/", fixture__menu__data, format="json")
        assert response.status_code == status.HTTP_201_CREATED
        assert response.json() == fixture__menu__data

    def test__update_view__without_authentication(self, api_client, fixture__menu):
        assert fixture__menu in Menu.objects.all()
        patch_data = {"description": "patched_description"}
        response = api_client.patch(f"/menu/menu/{fixture__menu.id}/", patch_data, format="json")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    @mock.patch('django.utils.timezone.now', mock.Mock(return_value=frozen_time_dt))
    @pytest.mark.parametrize("patch_data", [{"description": "patched_description"}, {"name": "patched_name"}])
    def test__update_view__with_authentication(
            self, api_client, fixture__menu__data, fixture__menu, fixture__user, patch_data
    ):
        assert fixture__menu in Menu.objects.all()
        api_client.force_authenticate(user=fixture__user)
        response = api_client.patch(f"/menu/menu/{fixture__menu.id}/", patch_data, format="json")
        expected_response_menu = fixture__menu__data
        expected_response_menu.update(patch_data)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_response_menu

    def test__destroy_view__without_authentication(self, api_client, fixture__menu):
        assert fixture__menu in Menu.objects.all()
        response = api_client.delete(f"/menu/menu/{fixture__menu.id}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert fixture__menu in Menu.objects.all()

    def test__destroy_view__with_authentication(self, api_client, fixture__user, fixture__menu):
        assert fixture__menu in Menu.objects.all()
        api_client.force_authenticate(user=fixture__user)
        response = api_client.delete(f"/menu/menu/{fixture__menu.id}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert fixture__menu not in Menu.objects.all()
