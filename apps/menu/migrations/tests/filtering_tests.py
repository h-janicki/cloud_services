from datetime import datetime
from unittest import mock

from rest_framework import status
import pytest

from apps.menu.models import Menu, MenuItem


@pytest.fixture
def fixture__menu_item__data() -> list:
    return [
        {
            "id": 1, "name": "test_name_1", "description": "test_description_1", "price": 50, "preparation_time": 50,
            "is_vegetarian": True, "created": "2020-02-08T15:02:03Z", "updated": "2020-02-08T15:02:03Z"
        },
    ]


@pytest.fixture
def fixture__menu__data() -> list:
    return [
        {"id": 1, "name": "test_name_a", "description": "test_description_a", "items": [1],
         "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"},
        {"id": 2, "name": "test_name_b", "description": "test_description_b", "items": [1],
         "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"},
        {"id": 3, "name": "test_name_c", "description": "test_description_c", "items": [1],
         "created": "2023-02-08T15:02:03Z", "updated": "2023-02-08T15:02:03Z"},
    ]


@pytest.fixture
def fixture__menu_item(fixture__menu_item__data) -> None:
    create_data = fixture__menu_item__data.copy()
    for menu_item_data in create_data:
        with mock.patch('django.utils.timezone.now', mock.Mock(
             return_value=datetime.strptime(menu_item_data["created"], "%Y-%m-%dT%H:%M:%SZ"))
        ):
            MenuItem.objects.create(**menu_item_data)
    yield


@pytest.fixture
def fixture__menu(fixture__menu_item, fixture__menu__data) -> None:
    create_data = fixture__menu__data.copy()
    for menu_data in create_data:
        items = menu_data.pop("items", None)
        with mock.patch('django.utils.timezone.now', mock.Mock(
             return_value=datetime.strptime(menu_data["created"], "%Y-%m-%dT%H:%M:%SZ"))
        ):
            menu = Menu.objects.create(**menu_data)
        if items:
            menu.items.set(items)
    yield


@pytest.mark.django_db
class UserViewSetListViewFilteringTest:

    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"name": "name_a"}, [{"id": 1, "name": "test_name_a", "description": "test_description_a", "items": [1],
                                   "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"}]],
            [{"name": "name_c"}, [{"id": 3, "name": "test_name_c", "description": "test_description_c", "items": [1],
                                   "created": "2023-02-08T15:02:03Z", "updated": "2023-02-08T15:02:03Z"}]],
        ]
    )
    def test__filtering__by_name(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result

    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"created__gt": "2022-02-08T15:02:03Z"}, [
                {"id": 3, "name": "test_name_c", "description": "test_description_c", "items": [1],
                 "created": "2023-02-08T15:02:03Z", "updated": "2023-02-08T15:02:03Z"}
            ]],
            [{"created__lt": "2022-02-08T15:02:03Z"}, [
                {"id": 1, "name": "test_name_a", "description": "test_description_a", "items": [1],
                 "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"}
            ]],
            [{"created__gt": "2021-02-08T15:02:03Z", "created__lt": "2023-02-08T15:02:03Z"}, [
                {"id": 2, "name": "test_name_b", "description": "test_description_b", "items": [1],
                 "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"},
            ]],
        ]
    )
    def test__filtering__by_created(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result

    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"updated__gt": "2021-02-08T15:02:03Z"}, [
                {"id": 2, "name": "test_name_b", "description": "test_description_b", "items": [1],
                 "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"},
                {"id": 3, "name": "test_name_c", "description": "test_description_c", "items": [1],
                 "created": "2023-02-08T15:02:03Z", "updated": "2023-02-08T15:02:03Z"}
            ]],
            [{"updated__lt": "2023-02-08T15:02:03Z"}, [
                {"id": 1, "name": "test_name_a", "description": "test_description_a", "items": [1],
                 "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"},
                {"id": 2, "name": "test_name_b", "description": "test_description_b", "items": [1],
                 "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"},
            ]],
            [{"updated__gt": "2021-02-08T15:02:03Z", "updated__lt": "2023-02-08T15:02:03Z"}, [
                {"id": 2, "name": "test_name_b", "description": "test_description_b", "items": [1],
                 "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"},
            ]],
        ]
    )
    def test__filtering__by_updated(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result
