from datetime import datetime
from unittest import mock

from rest_framework import status
import pytest

from apps.menu.models import MenuItem, Menu


@pytest.fixture
def fixture__menu_item__data() -> list:
    return [
        {
            "id": 1, "name": "test_name_1", "description": "test_description_1", "price": 50, "preparation_time": 50,
            "is_vegetarian": True, "created": "2020-02-08T15:02:03Z", "updated": "2020-02-08T15:02:03Z"
        },
        {
            "id": 2, "name": "test_name_2", "description": "test_description_2", "price": 50, "preparation_time": 50,
            "is_vegetarian": True, "created": "2020-02-08T15:02:03Z", "updated": "2020-02-08T15:02:03Z"
        },
    ]


@pytest.fixture
def fixture__menu__data() -> list:
    return [
        {
            "id": 1, "name": "test_name_a", "description": "test_description_a",
            "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
        },
        {
           "id": 2, "name": "test_name_b", "description": "test_description_b",
           "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
        },
    ]


@pytest.fixture
def fixture__menu_item(fixture__menu_item__data) -> None:
    create_data = fixture__menu_item__data.copy()
    for menu_item_data in create_data:
        with mock.patch('django.utils.timezone.now', mock.Mock(
             return_value=datetime.strptime(menu_item_data["created"], "%Y-%m-%dT%H:%M:%SZ"))
        ):
            MenuItem.objects.create(**menu_item_data)
    yield


@pytest.fixture
def fixture__menu(fixture__menu_item, fixture__menu__data) -> None:
    create_data = fixture__menu__data.copy()
    for menu_data in create_data:
        items = menu_data.pop("items", None)
        with mock.patch('django.utils.timezone.now', mock.Mock(
             return_value=datetime.strptime(menu_data["created"], "%Y-%m-%dT%H:%M:%SZ"))
        ):
            menu = Menu.objects.create(**menu_data)
        if items:
            menu.items.set(items)
    yield


@pytest.mark.django_db
class UserViewSetListViewOrderingTest:

    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"order_by": "items_count"}, [
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
            ]],
            [{"order_by": "-items_count"}, [
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
            ]],
        ]
    )
    def test__ordering__by_items_count(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result


    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"order_by": "name"}, [
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
            ]],
            [{"order_by": "-name"}, [
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
            ]],
        ]
    )
    def test__ordering__by_name(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result


    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"order_by": "created"}, [
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
            ]],
            [{"order_by": "-created"}, [
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
            ]],
        ]
    )
    def test__ordering__by_created(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result


    @pytest.mark.parametrize(
        "query_params,expected_result",
        [
            [{"order_by": "updated"}, [
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
            ]],
            [{"order_by": "-updated"}, [
                {
                    "id": 2, "name": "test_name_b", "description": "test_description_b",
                    "items": [1], "created": "2022-02-08T15:02:03Z", "updated": "2022-02-08T15:02:03Z"
                },
                {
                    "id": 1, "name": "test_name_a", "description": "test_description_a",
                    "items": [1, 2], "created": "2021-02-08T15:02:03Z", "updated": "2021-02-08T15:02:03Z"
                },
            ]],
        ]
    )
    def test__ordering__by_updated(self, api_client, fixture__menu, query_params, expected_result):
        response = api_client.get("/menu/menu/", query_params)
        assert response.status_code == status.HTTP_200_OK
        assert response.json() == expected_result
