from django.urls import include, path
from rest_framework.routers import DefaultRouter

from apps.menu.views import MenuViewSet


router = DefaultRouter()

router.register(r"menu", MenuViewSet, basename="menu")

urlpatterns = [
    path('', include(router.urls)),
]
