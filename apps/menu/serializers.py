from django.db import transaction
from rest_framework import serializers

from apps.menu.models import Menu, MenuItem


class MenuItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuItem
        fields = "__all__"
        read_only_fields = ("id", "created", "updated")


class MenuSerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(many=True, queryset=MenuItem.objects.all())

    class Meta:
        model = Menu
        fields = "__all__"
        read_only_fields = ("id",)

    @transaction.atomic
    def create(self, validated_data):
        items = validated_data.pop("items", None)
        obj = super().create(validated_data)
        if items:
            obj.items.set(items)
        return obj

    @transaction.atomic
    def update(self, obj, validated_data):
        items = validated_data.pop("items", None)
        obj = super().update(obj, validated_data)
        if items:
            obj.items.set(items)
        return obj


class MenuDetailSerializer(MenuSerializer):
    items = MenuItemSerializer(read_only=True, many=True)
