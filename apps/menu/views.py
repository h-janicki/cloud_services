from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from apps.menu.filters import MenuFilterSet
from apps.menu.models import Menu
from apps.menu.serializers import MenuSerializer, MenuDetailSerializer


class MenuViewSet(viewsets.ModelViewSet):
    queryset = Menu.objects.filter(items__isnull=False).distinct()
    permission_classes = [IsAuthenticatedOrReadOnly]
    filterset_class = MenuFilterSet

    def get_serializer_class(self):
        if self.action == "retrieve":
            return MenuDetailSerializer
        return MenuSerializer
