from typing import Generator

from django.contrib.auth.models import User
from rest_framework.test import APIClient
import pytest


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def fixture__user() -> Generator[User, None, None]:
    user = User.objects.create_user(username="test_user", password="test_password")
    yield user
