FROM python:3.10-buster

# set environment variables
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    DockerHOME=/home/app

RUN mkdir -p $DockerHOME
COPY . $DockerHOME

WORKDIR $DockerHOME

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD python manage.py runserver 0.0.0.0:8000